# ECDS (EClub Design System)

ECDS es el sistema de diseño utilizado por EClub Mobile App, proporcionando un conjunto coherente y
reutilizable de componentes de diseño.
ECDS (EClub Design System) es un paquete de Dart/Flutter que encapsula componentes de diseño
reutilizables, colores, y tipografías para proyectos de Flutter. Facilita la implementación de una
interfaz de usuario coherente y atractiva acorde con los estándares de diseño de EClub.

## Características

- **Colores**: Una paleta de colores prediseñada que garantiza consistencia visual en toda la
  aplicación.
- **Tipografía**: Estilos de texto definidos para diferentes usos (títulos, subtítulos, cuerpo de
  texto) que mejoran la legibilidad y la estética.
- **Componentes UI**: Conjunto de widgets y componentes UI reutilizables que aceleran el proceso de
  desarrollo.

## Instalación

Para integrar ECDS en tu proyecto Flutter, puedes añadirlo como dependencia en tu
archivo `pubspec.yaml`:

```yaml
dependencies:
  ecds:
    git: 'https://gitlab.com/eclub_test/ecds.git'
```

O, si prefieres trabajar localmente, clona el repositorio y apunta la dependencia a tu copia local:

```yaml
dependencies:
  ecds:
    path: ../path/to/your/local/ecds
```

## Uso

Una vez instalado, puedes comenzar a utilizar los componentes de ECDS en tu proyecto importando el
paquete:

```dart
import 'package:ecds/ecds.dart';
```

Utiliza los componentes disponibles para construir tu UI, aprovechando la consistencia y
reutilización que ofrece ECDS.

## Componentes Principales

A continuación, se detallan algunos de los componentes clave que ofrece ECDS, junto con sus clases y
subclases principales:

### Botones

- `EClubBtnSize`: Define tamaños preestablecidos para los botones como `xs`, `sm`, `md`, `lg`,
  y `xl`. Cada tamaño especifica el padding horizontal y vertical, así como el estilo de texto a
  utilizar.
- `EClubBtnInterface`: Permite configurar propiedades comunes de los botones, incluyendo color,
  tamaño, borde, y sombra.

### Textos

ECDS proporciona una variedad de widgets de texto preconfigurados para diferentes jerarquías
tipográficas, incluyendo:

- `EcTextHeading1`, `EcTextHeading2`, `EcTextHeading3`, `EcTextHeading4`, `EcTextHeading5`, `EcTextHeading6`:
  Para títulos y encabezados.
- `EcTextMedium`, `EcTextSmall`, `EcTextCaptionS`, `EcTextCaptionXs`: Para texto de cuerpo,
  descripciones y pie de foto.
- `EcTextNumberL`, `EcTextNumberS`: Específicos para mostrar números, utilizando la
  fuente `RobotoMono`.

### Colores

`EClubColorsInterface` proporciona un acceso conveniente a la paleta de colores de EClub, incluyendo
colores primarios, secundarios, de fondo, entre otros. Estos colores pueden ser utilizados para
mantener la coherencia visual a lo largo de la aplicación.

## Uso

Para comenzar a utilizar ECDS en tu proyecto Flutter, primero debes añadirlo como dependencia. Esto
se puede hacer directamente desde un repositorio Git:





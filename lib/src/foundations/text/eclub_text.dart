part of 'text.dart';

class ECTextHeading1 extends StatelessWidget {
  const ECTextHeading1(
    this.label, {
    super.key,
    this.textAlign,
    this.weight,
    this.color,
    this.textOverflow,
    this.letterSpacing,
    this.maxLines,
    this.decoration,
  });

  final String label;
  final TextAlign? textAlign;
  final FontWeight? weight;
  final Color? color;
  final TextOverflow? textOverflow;
  final double? letterSpacing;
  final int? maxLines;
  final TextDecoration? decoration;

  @override
  Widget build(BuildContext context) {
    return _TextGeneric(
      key: key,
      label: label,
      fontSize: EClubTypography.heading1,
      color: color ?? Ecds.colors.textColor,
      fontStyle: FontStyle.normal,
      fontWeight: weight ?? FontWeight.w700,
      textAlign: textAlign,
      textOverflow: textOverflow,
      letterSpacing: letterSpacing,
      maxLines: maxLines,
      decoration: decoration,
    );
  }
}

class EcTextHeading2 extends StatelessWidget {
  /// Creates a eClub heading text widget.
  ///
  /// the default size of this widget is 28

  const EcTextHeading2(
    this.label, {
    super.key,
    this.textAlign,
    this.weight,
    this.color,
    this.textOverflow,
    this.letterSpacing,
    this.maxLines,
    this.decoration,
    this.shadows,
  });

  final String label;
  final TextAlign? textAlign;
  final FontWeight? weight;
  final Color? color;
  final TextOverflow? textOverflow;
  final double? letterSpacing;
  final int? maxLines;
  final TextDecoration? decoration;
  final List<Shadow>? shadows;

  @override
  Widget build(BuildContext context) {
    return _TextGeneric(
      key: key,
      label: label,
      fontSize: EClubTypography.heading2,
      color: color ?? Ecds.colors.textColor,
      fontStyle: FontStyle.normal,
      fontWeight: weight ?? FontWeight.w700,
      textAlign: textAlign,
      textOverflow: textOverflow,
      letterSpacing: letterSpacing,
      maxLines: maxLines,
      decoration: decoration,
      shadows: shadows,
    );
  }
}

class EcTextHeading3 extends StatelessWidget {
  /// Creates a eClub heading text widget.
  ///
  /// the default size of this widget is 24

  const EcTextHeading3(
    this.label, {
    super.key,
    this.textAlign,
    this.weight,
    this.color,
    this.textOverflow,
    this.letterSpacing,
    this.maxLines,
    this.decoration,
    this.shadows,
  });

  final String label;
  final TextAlign? textAlign;
  final FontWeight? weight;
  final Color? color;
  final TextOverflow? textOverflow;
  final double? letterSpacing;
  final int? maxLines;
  final TextDecoration? decoration;
  final List<Shadow>? shadows;

  @override
  Widget build(BuildContext context) {
    return _TextGeneric(
      key: key,
      label: label,
      fontSize: EClubTypography.heading3,
      color: color ?? Ecds.colors.textColor,
      fontStyle: FontStyle.normal,
      fontWeight: weight ?? FontWeight.w700,
      textAlign: textAlign,
      textOverflow: textOverflow,
      letterSpacing: letterSpacing,
      maxLines: maxLines,
      decoration: decoration,
      shadows: shadows,
    );
  }
}

class EcTextHeading4 extends StatelessWidget {
  /// Creates a eClub heading text widget.
  ///
  /// the default size of this widget is 20

  const EcTextHeading4(
    this.label, {
    super.key,
    this.textAlign,
    this.weight,
    this.color,
    this.textOverflow,
    this.letterSpacing,
    this.maxLines,
    this.decoration,
    this.shadows,
  });

  final String label;
  final TextAlign? textAlign;
  final FontWeight? weight;
  final Color? color;
  final TextOverflow? textOverflow;
  final double? letterSpacing;
  final int? maxLines;
  final TextDecoration? decoration;
  final List<Shadow>? shadows;

  @override
  Widget build(BuildContext context) {
    return _TextGeneric(
      key: key,
      label: label,
      fontSize: EClubTypography.heading4,
      color: color ?? Ecds.colors.textColor,
      fontStyle: FontStyle.normal,
      fontWeight: weight ?? FontWeight.w700,
      textAlign: textAlign,
      textOverflow: textOverflow,
      letterSpacing: letterSpacing,
      maxLines: maxLines,
      decoration: decoration,
      shadows: shadows,
    );
  }
}

class EcTextHeading5 extends StatelessWidget {
  /// Creates a eClub heading text widget.
  ///
  /// the default size of this widget is 18

  const EcTextHeading5(
    this.label, {
    super.key,
    this.textAlign,
    this.weight,
    this.color,
    this.textOverflow,
    this.letterSpacing,
    this.maxLines,
    this.decoration,
    this.shadows,
  });

  final String label;
  final TextAlign? textAlign;
  final FontWeight? weight;
  final Color? color;
  final TextOverflow? textOverflow;
  final double? letterSpacing;
  final int? maxLines;
  final TextDecoration? decoration;
  final List<Shadow>? shadows;

  @override
  Widget build(BuildContext context) {
    return _TextGeneric(
      key: key,
      label: label,
      fontSize: EClubTypography.heading5,
      color: color ?? Ecds.colors.textColor,
      fontStyle: FontStyle.normal,
      fontWeight: weight ?? FontWeight.w700,
      textAlign: textAlign,
      textOverflow: textOverflow,
      letterSpacing: letterSpacing,
      maxLines: maxLines,
      decoration: decoration,
      shadows: shadows,
    );
  }
}

class EcTextHeading6 extends StatelessWidget {
  /// Creates a eClub heading text widget.
  ///
  /// the default size of this widget is 16

  const EcTextHeading6(
    this.label, {
    super.key,
    this.textAlign,
    this.weight,
    this.color,
    this.textOverflow,
    this.letterSpacing,
    this.maxLines,
    this.decoration,
    this.shadows,
  });

  final String label;
  final TextAlign? textAlign;
  final FontWeight? weight;
  final Color? color;
  final TextOverflow? textOverflow;
  final double? letterSpacing;
  final int? maxLines;
  final TextDecoration? decoration;
  final List<Shadow>? shadows;

  @override
  Widget build(BuildContext context) {
    return _TextGeneric(
      key: key,
      label: label,
      fontSize: EClubTypography.heading6,
      color: color ?? Ecds.colors.textColor,
      fontStyle: FontStyle.normal,
      fontWeight: weight ?? FontWeight.w600,
      textAlign: textAlign,
      textOverflow: textOverflow,
      letterSpacing: letterSpacing,
      maxLines: maxLines,
      decoration: decoration,
      shadows: shadows,
    );
  }
}

class EcTextMedium extends StatelessWidget {
  /// Creates a eClub Text Widget based on eClub Design system
  ///
  ///  - The default `FontSize` for this widget is `12` and uses
  /// `NunitoSans` as a `fontFamily`
  ///
  /// - The default weight is `w400`
  /// ```dart
  /// eClubTextMedium('eClub text here');
  /// ```
  const EcTextMedium(
    this.label, {
    super.key,
    this.textAlign,
    this.weight,
    this.color,
    this.textOverflow,
    this.letterSpacing,
    this.maxLines,
    this.decoration,
    this.shadows,
  });

  final String label;
  final TextAlign? textAlign;
  final FontWeight? weight;
  final Color? color;
  final TextOverflow? textOverflow;
  final double? letterSpacing;
  final int? maxLines;
  final TextDecoration? decoration;
  final List<Shadow>? shadows;

  @override
  Widget build(BuildContext context) {
    return _TextGeneric(
      key: key,
      label: label,
      fontSize: EClubTypography.medium,
      color: color ?? Ecds.colors.textColor,
      fontStyle: FontStyle.normal,
      fontWeight: weight ?? FontWeight.w400,
      textAlign: textAlign,
      textOverflow: textOverflow,
      letterSpacing: letterSpacing,
      maxLines: maxLines,
      decoration: decoration,
      shadows: shadows,
    );
  }
}

class EcTextSmall extends StatelessWidget {
  /// Creates a eClub Text Widget based on eClub Design system
  ///
  ///  - The default `FontSize` for this widget is `10` and uses
  /// `NunitoSans` as a `fontFamily`
  ///
  /// - The default weight is `w400`
  /// ```dart
  /// eClubTextSmall('eClub text here');
  /// ```
  const EcTextSmall(
    this.label, {
    super.key,
    this.textAlign,
    this.weight,
    this.color,
    this.textOverflow,
    this.letterSpacing,
    this.maxLines,
    this.decoration,
    this.shadows,
  });

  final String label;
  final TextAlign? textAlign;
  final FontWeight? weight;
  final Color? color;
  final TextOverflow? textOverflow;
  final double? letterSpacing;
  final int? maxLines;
  final TextDecoration? decoration;
  final List<Shadow>? shadows;

  @override
  Widget build(BuildContext context) {
    return _TextGeneric(
      key: key,
      label: label,
      fontSize: EClubTypography.captionSmall,
      color: color ?? Ecds.colors.textColor,
      fontStyle: FontStyle.normal,
      fontWeight: weight ?? FontWeight.w400,
      textAlign: textAlign,
      textOverflow: textOverflow,
      letterSpacing: letterSpacing,
      maxLines: maxLines,
      decoration: decoration,
      shadows: shadows,
    );
  }
}

class EcTextCaptionS extends StatelessWidget {
  /// Creates a eClub Text Widget based on eClub Design system
  ///
  ///  - The default `FontSize` for this widget is `12` and uses
  /// `NunitoSans` as a `fontFamily`
  ///
  /// - The default weight is `w400`
  /// ```dart
  /// eClubTextCaptionS('eClub text here');
  /// ```
  const EcTextCaptionS(
    this.label, {
    super.key,
    this.textAlign,
    this.weight,
    this.color,
    this.textOverflow,
    this.letterSpacing,
    this.maxLines,
    this.decoration,
    this.shadows,
  });

  final String label;
  final TextAlign? textAlign;
  final FontWeight? weight;
  final Color? color;
  final TextOverflow? textOverflow;
  final double? letterSpacing;
  final int? maxLines;
  final TextDecoration? decoration;
  final List<Shadow>? shadows;

  @override
  Widget build(BuildContext context) {
    return _TextGeneric(
      key: key,
      label: label,
      fontSize: EClubTypography.captionSmall,
      color: color ?? Ecds.colors.textColor,
      fontStyle: FontStyle.normal,
      fontWeight: weight ?? FontWeight.w400,
      textAlign: textAlign,
      textOverflow: textOverflow,
      letterSpacing: letterSpacing,
      maxLines: maxLines,
      decoration: decoration,
      shadows: shadows,
    );
  }
}

class EcTextCaptionXs extends StatelessWidget {
  /// Creates a eClub Text Widget based on eClub Design system
  ///
  ///  - The default `FontSize` for this widget is `10` and uses
  /// `NunitoSans` as a `fontFamily`
  ///
  /// - The default weight is `w400`
  /// ```dart
  /// eClubTextCaptionXs('eClub text here');
  /// ```
  const EcTextCaptionXs(
    this.label, {
    super.key,
    this.textAlign,
    this.weight,
    this.color,
    this.textOverflow,
    this.letterSpacing,
    this.maxLines,
    this.decoration,
    this.shadows,
  });

  final String label;
  final TextAlign? textAlign;
  final FontWeight? weight;
  final Color? color;
  final TextOverflow? textOverflow;
  final double? letterSpacing;
  final int? maxLines;
  final TextDecoration? decoration;
  final List<Shadow>? shadows;

  @override
  Widget build(BuildContext context) {
    return _TextGeneric(
      key: key,
      label: label,
      fontSize: EClubTypography.captionExtraSmall,
      color: color ?? Ecds.colors.textColor,
      fontStyle: FontStyle.normal,
      fontWeight: weight ?? FontWeight.w400,
      textAlign: textAlign,
      textOverflow: textOverflow,
      letterSpacing: letterSpacing,
      maxLines: maxLines,
      decoration: decoration,
    );
  }
}

class EcTextNumberL extends StatelessWidget {
  /// Creates a eClub Text Widget based on eClub Design system
  ///
  ///  - The default `FontSize` for this widget is `18` and uses
  /// `RobotoMono` as a `fontFamily`
  ///
  /// - The default weight is `Bold` or `w400`
  /// ```dart
  /// eClubTextNumberL('$ 5.600.00');
  /// ```
  const EcTextNumberL(
    this.label, {
    super.key,
    this.textAlign,
    this.weight,
    this.color,
    this.textOverflow,
    this.letterSpacing,
    this.maxLines,
    this.decoration,
  });

  final String label;
  final TextAlign? textAlign;
  final FontWeight? weight;
  final Color? color;
  final TextOverflow? textOverflow;
  final double? letterSpacing;
  final int? maxLines;
  final TextDecoration? decoration;

  @override
  Widget build(BuildContext context) {
    return _TextGeneric(
      key: key,
      label: label,
      fontSize: EClubTypography.heading5,
      color: color ?? Ecds.colors.textColor,
      fontStyle: FontStyle.normal,
      fontWeight: weight ?? FontWeight.w400,
      textAlign: textAlign,
      textOverflow: textOverflow,
      letterSpacing: letterSpacing,
      maxLines: maxLines,
      decoration: decoration,
      fontFamily: EClubTypography.robotoMono,
    );
  }
}

class EcTextNumberS extends StatelessWidget {
  /// Creates a eClub Text Widget based on eClub Design system
  ///
  ///  - The default `FontSize` for this widget is `12` and uses
  /// `RobotoMono` as a `fontFamily`
  ///
  /// - The default weight is `Bold` or `w400`
  /// ```dart
  /// eClubTextNumberS('$ 5.600.00')
  /// ```
  const EcTextNumberS(
    this.label, {
    super.key,
    this.textAlign,
    this.weight,
    this.color,
    this.textOverflow,
    this.letterSpacing,
    this.maxLines,
    this.decoration,
  });

  final String label;
  final TextAlign? textAlign;
  final FontWeight? weight;
  final Color? color;
  final TextOverflow? textOverflow;
  final double? letterSpacing;
  final int? maxLines;
  final TextDecoration? decoration;

  @override
  Widget build(BuildContext context) {
    return _TextGeneric(
      key: key,
      label: label,
      fontSize: EClubTypography.captionSmall,
      color: color ?? Ecds.colors.textColor,
      fontStyle: FontStyle.normal,
      fontWeight: weight ?? FontWeight.w400,
      textAlign: textAlign,
      textOverflow: textOverflow,
      letterSpacing: letterSpacing,
      maxLines: maxLines,
      decoration: decoration,
      fontFamily: EClubTypography.robotoMono,
    );
  }
}

class EcTextCustom extends StatelessWidget {
  /// Creates a eClub custom text widget.
  ///
  /// the custom size

  const EcTextCustom(
    this.label, {
    super.key,
    required this.fontSize,
    this.textAlign,
    this.weight,
    this.color,
    this.textOverflow,
    this.letterSpacing,
    this.maxLines,
    this.decoration,
    this.fontStyle,
    this.shadows,
    this.fontFamily,
  });

  final String label;
  final TextAlign? textAlign;
  final FontWeight? weight;
  final Color? color;
  final TextOverflow? textOverflow;
  final double? letterSpacing;
  final int? maxLines;
  final TextDecoration? decoration;
  final double fontSize;
  final FontStyle? fontStyle;
  final List<Shadow>? shadows;
  final String? fontFamily;

  @override
  Widget build(BuildContext context) {
    return _TextGeneric(
      key: key,
      label: label,
      fontSize: fontSize,
      color: color ?? Ecds.colors.textColor,
      fontStyle: fontStyle ?? FontStyle.normal,
      fontWeight: weight ?? FontWeight.w600,
      textAlign: textAlign,
      textOverflow: textOverflow,
      letterSpacing: letterSpacing,
      maxLines: maxLines,
      fontFamily: fontFamily,
      decoration: decoration,
      shadows: shadows,
    );
  }
}

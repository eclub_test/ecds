import 'dart:math';

import 'package:ecds/src/init.dart';
import 'package:ecds/src/tokens/typography.dart';
import 'package:flutter/material.dart';

part 'eclub_text.dart';

class EClubText {
  EClubText._();

  ///h1 Text widget - fontSize 96
  static Widget h1({
    required String label,
    Key? key,
    TextAlign? textAlign,
    FontWeight? fontWeight,
    Color? color,
    TextOverflow? textOverflow,
    double? letterSpacing,
    int? maxLines,
    TextStyle? textStyle,
    TextDecoration? decoration,
  }) =>
      _TextGeneric(
        key: key,
        label: label,
        fontSize: EClubTypography.heading1,
        color: color ?? Ecds.colors.black,
        fontStyle: FontStyle.normal,
        fontWeight: fontWeight ?? FontWeight.normal,
        textAlign: textAlign,
        textOverflow: textOverflow,
        letterSpacing: letterSpacing,
        maxLines: maxLines,
        textStyle: textStyle,
        decoration: decoration,
      );

  ///h2 Text widget - fontSize 58
  static Widget h2({
    required String label,
    Key? key,
    TextAlign? textAlign,
    FontWeight? fontWeight,
    Color? color,
    TextOverflow? textOverflow,
    double? letterSpacing,
    int? maxLines,
    TextStyle? textStyle,
    TextDecoration? decoration,
  }) =>
      _TextGeneric(
        key: key,
        label: label,
        fontSize: EClubTypography.heading2,
        color: color ?? Ecds.colors.black,
        fontStyle: FontStyle.normal,
        fontWeight: fontWeight ?? FontWeight.normal,
        textAlign: textAlign,
        textOverflow: textOverflow,
        letterSpacing: letterSpacing,
        maxLines: maxLines,
        textStyle: textStyle,
        decoration: decoration,
      );

  ///h3 Text widget - fontSize 47
  static Widget h3({
    required String label,
    Key? key,
    TextAlign? textAlign,
    FontWeight? fontWeight,
    Color? color,
    TextOverflow? textOverflow,
    double? letterSpacing,
    int? maxLines,
    TextStyle? textStyle,
    TextDecoration? decoration,
  }) =>
      _TextGeneric(
        key: key,
        label: label,
        fontSize: EClubTypography.heading3,
        color: color ?? Ecds.colors.black,
        fontStyle: FontStyle.normal,
        fontWeight: fontWeight ?? FontWeight.normal,
        textAlign: textAlign,
        textOverflow: textOverflow,
        letterSpacing: letterSpacing,
        maxLines: maxLines,
        textStyle: textStyle,
        decoration: decoration,
      );

  ///h4 Text widget - fontSize 33
  static Widget h4({
    required String label,
    Key? key,
    TextAlign? textAlign,
    FontWeight? fontWeight,
    Color? color,
    TextOverflow? textOverflow,
    double? letterSpacing,
    int? maxLines,
    TextStyle? textStyle,
    TextDecoration? decoration,
  }) =>
      _TextGeneric(
        key: key,
        label: label,
        fontSize: EClubTypography.heading4,
        color: color ?? Ecds.colors.black,
        fontStyle: FontStyle.normal,
        fontWeight: fontWeight ?? FontWeight.normal,
        textAlign: textAlign,
        textOverflow: textOverflow,
        letterSpacing: letterSpacing,
        maxLines: maxLines,
        textStyle: textStyle,
        decoration: decoration,
      );

  ///h5 Text widget - fontSize 23
  static Widget h5({
    required String label,
    Key? key,
    TextAlign? textAlign,
    FontWeight? fontWeight,
    Color? color,
    TextOverflow? textOverflow,
    double? letterSpacing,
    int? maxLines,
    TextStyle? textStyle,
    TextDecoration? decoration,
  }) =>
      _TextGeneric(
        key: key,
        label: label,
        fontSize: EClubTypography.heading5,
        color: color ?? Ecds.colors.black,
        fontStyle: FontStyle.normal,
        fontWeight: fontWeight ?? FontWeight.normal,
        textAlign: textAlign,
        textOverflow: textOverflow,
        letterSpacing: letterSpacing,
        maxLines: maxLines,
        textStyle: textStyle,
        decoration: decoration,
      );

  ///h6 Text widget - fontSize 19
  static Widget h6({
    required String label,
    Key? key,
    TextAlign? textAlign,
    FontWeight? fontWeight,
    Color? color,
    TextOverflow? textOverflow,
    double? letterSpacing,
    int? maxLines,
    TextStyle? textStyle,
    TextDecoration? decoration,
  }) =>
      _TextGeneric(
        key: key,
        label: label,
        fontSize: EClubTypography.heading6,
        color: color ?? Ecds.colors.black,
        fontStyle: FontStyle.normal,
        fontWeight: fontWeight ?? FontWeight.bold,
        textAlign: textAlign,
        textOverflow: textOverflow,
        letterSpacing: letterSpacing,
        maxLines: maxLines,
        textStyle: textStyle,
        decoration: decoration,
      );

  ///body Text widget - fontSize 16
  static Widget body({
    required String label,
    Key? key,
    TextAlign? textAlign,
    FontWeight? fontWeight,
    Color? color,
    TextOverflow? textOverflow,
    int? maxLines,
    double? letterSpacing,
    TextStyle? textStyle,
    TextDecoration? decoration,
  }) =>
      _TextGeneric(
        key: key,
        label: label,
        fontSize: EClubTypography.heading6,
        color: color ?? Ecds.colors.black,
        fontStyle: FontStyle.normal,
        fontWeight: fontWeight ?? FontWeight.normal,
        textAlign: textAlign,
        textOverflow: textOverflow,
        maxLines: maxLines,
        letterSpacing: letterSpacing,
        textStyle: textStyle,
        decoration: decoration,
      );

  ///title Text widget - fontSize 18
  static Widget title({
    required String label,
    Key? key,
    TextAlign? textAlign,
    FontWeight? fontWeight,
    Color? color,
    TextOverflow? textOverflow,
    int? maxLines,
    double? letterSpacing,
    TextStyle? textStyle,
  }) =>
      _TextGeneric(
        key: key,
        label: label,
        fontSize: EClubTypography.heading5,
        color: color ?? Ecds.colors.black,
        fontStyle: FontStyle.normal,
        fontWeight: fontWeight ?? FontWeight.normal,
        textAlign: textAlign,
        textOverflow: textOverflow,
        maxLines: maxLines,
        letterSpacing: letterSpacing,
        textStyle: textStyle,
      );

  ///labelText Text widget - fontSize 14
  static Widget labelText({
    required String label,
    Key? key,
    TextAlign? textAlign,
    FontWeight? fontWeight,
    Color? color,
    TextOverflow? textOverflow,
    int? maxLines,
    double? letterSpacing,
    TextStyle? textStyle,
    TextDecoration? decoration,
  }) =>
      _TextGeneric(
        key: key,
        label: label,
        fontSize: EClubTypography.label,
        color: color ?? Ecds.colors.black,
        fontStyle: FontStyle.normal,
        fontWeight: fontWeight ?? FontWeight.normal,
        textAlign: textAlign,
        textOverflow: textOverflow,
        maxLines: maxLines,
        letterSpacing: letterSpacing,
        textStyle: textStyle,
        decoration: decoration,
      );

  static Widget small({
    required String label,
    Key? key,
    TextAlign? textAlign,
    FontWeight? fontWeight,
    Color? color,
    TextOverflow? textOverflow,
    int? maxLines,
    double? letterSpacing,
    TextStyle? textStyle,
    TextDecoration? decoration,
  }) =>
      _TextGeneric(
        key: key,
        label: label,
        fontSize: EClubTypography.small,
        color: color ?? Ecds.colors.primaryColor,
        fontStyle: FontStyle.normal,
        fontWeight: fontWeight ?? FontWeight.normal,
        textAlign: textAlign,
        textOverflow: textOverflow,
        maxLines: maxLines,
        letterSpacing: letterSpacing,
        textStyle: textStyle,
        decoration: decoration,
      );



  static TextStyle style({
    double? fontSize,
    Color? color,
    FontWeight? fontWeight,
    FontStyle? fontStyle,
    double? letterSpacing,
    TextStyle? textStyle,
    TextDecoration? decoration,
    double? lineHeight,
    List<Shadow>? shadows,
  }) =>
      TextStyle(
        height: lineHeight,
        fontFamily: Ecds.typography.font,
        color: color,
        fontSize: fontSize,
        fontWeight: fontWeight,
        fontStyle: fontStyle ?? FontStyle.normal,
        letterSpacing: letterSpacing,
        decoration: decoration ?? TextDecoration.none,
        shadows: shadows,
      ).merge(textStyle);

  static String _generateTag(int len) {
    final random = Random();
    final result = String.fromCharCodes(
      List.generate(len, (index) => random.nextInt(33) + 89),
    );

    return result;
  }
}

class _TextGeneric extends StatelessWidget {
  const _TextGeneric({
    required this.label,
    required this.fontSize,
    super.key,
    this.color,
    this.fontStyle,
    this.fontWeight,
    this.textOverflow,
    this.textAlign,
    this.letterSpacing,
    this.maxLines,
    this.textStyle,
    this.decoration,
    this.fontFamily,
    this.shadows,
  });

  final String label;
  final double fontSize;
  final TextOverflow? textOverflow;
  final Color? color;
  final FontWeight? fontWeight;
  final FontStyle? fontStyle;
  final TextAlign? textAlign;
  final double? letterSpacing;
  final int? maxLines;
  final TextStyle? textStyle;
  final TextDecoration? decoration;
  final String? fontFamily;
  final List<Shadow>? shadows;

  @override
  Widget build(BuildContext context) {
    return Text(
      label,
      textAlign: textAlign,
      maxLines: maxLines,
      style: TextStyle(
        fontFamily: fontFamily ,
        package: 'mobile_tds',
        color: color,
        fontSize: fontSize,
        fontWeight: fontWeight,
        fontStyle: fontStyle ?? FontStyle.normal,
        letterSpacing: letterSpacing,
        decoration: decoration ?? TextDecoration.none,
        overflow: textOverflow,
        shadows: shadows,
      ).merge(textStyle),
    );
  }
}

import 'package:ecds/src/tokens/colors.dart';
import 'package:ecds/src/tokens/tokens_export.dart';
import 'package:flutter/material.dart';

class EClubColorsInterface {
  const EClubColorsInterface({
    this.black = EClubColors.black,
    this.white = EClubColors.white,
    this.textColor = EClubColors.black,
    this.primaryColor = EClubColors.raspberry,
    this.tertiaryColor = EClubColors.tertiary,
    this.background = EClubColors.background,
    this.redCrayola = EClubColors.redCrayola,
    this.orangePeel = EClubColors.orangePeel,
    this.paleDogwood = EClubColors.paleDogwood,
    this.lightRed = EClubColors.lightRed,
    this.secondary = EClubColors.secondary,
    this.foodCategory = EClubColors.foodCategory,
    this.shopsCategory = EClubColors.shopsCategory,
    this.transportCategory = EClubColors.transportCategory,
    this.healthCategory = EClubColors.healthCategory,
    this.atmCategory = EClubColors.atmCategory,
    this.entertainmentCategory = EClubColors.entertainmentCategory,
  });

  final Color black;
  final Color secondary;
  final Color white;
  final Color textColor;
  final Color primaryColor;
  final Color tertiaryColor;
  final Color background;
  final Color redCrayola;
  final Color orangePeel;
  final Color paleDogwood;
  final Color lightRed;
  final Color foodCategory;
  final Color shopsCategory;
  final Color transportCategory;
  final Color healthCategory;
  final Color atmCategory;
  final Color entertainmentCategory;
}

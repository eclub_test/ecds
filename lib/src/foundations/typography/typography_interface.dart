import 'package:ecds/src/tokens/typography.dart';

class EClubTypographyInterface {
  const EClubTypographyInterface({
    this.font = EClubTypography.nunitoSans,
    this.h1 = EClubTypography.heading1,
    this.h2 = EClubTypography.heading2,
    this.h3 = EClubTypography.heading3,
    this.h4 = EClubTypography.heading4,
    this.h5 = EClubTypography.heading5,
    this.h6 = EClubTypography.heading6,
    this.body = EClubTypography.heading6,
    this.label = EClubTypography.label,
  });

  final String font;
  final double h1;
  final double h2;
  final double h3;
  final double h4;
  final double h5;
  final double h6;
  final double body;
  final double label;
}

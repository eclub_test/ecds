class EClubTypography {
  const EClubTypography._();

  /// Typographies
  static const String nunitoSans = 'NunitoSans';
  static const String inter = 'Inter';
  static const String robotoMono = 'RobotoMono';
  static const double heading1 = 32;
  static const double heading2 = 28;
  static const double heading3 = 24;
  static const double heading4 = 20;
  static const double heading5 = 18;
  static const double heading6 = 16;
  static const double label = 14;
  static const double medium = 12;
  static const double captionSmall = 12;
  static const double small = 12;
  static const double captionExtraSmall = 10;
}

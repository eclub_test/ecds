//  ..................................................
//  .██████╗.██████╗.██╗......██████╗.██████╗.███████╗
//  ██╔════╝██╔═══██╗██║.....██╔═══██╗██╔══██╗██╔════╝
//  ██║.....██║...██║██║.....██║...██║██████╔╝███████╗
//  ██║.....██║...██║██║.....██║...██║██╔══██╗╚════██║
//  ╚██████╗╚██████╔╝███████╗╚██████╔╝██║..██║███████║
//  .╚═════╝.╚═════╝.╚══════╝.╚═════╝.╚═╝..╚═╝╚══════╝
//  ..................................................

import 'package:flutter/material.dart';

class EClubColors {
  const EClubColors._();

  static const Color black = Color(0xFF000000);
  static const Color white = Color(0xFFFFFFFF);
  static const Color isabelline = Color(0xFFF6F1ED);
  static const Color raspberry = Color(0xFFDF0A5D);
  static const Color redCrayola = Color(0xFFF00E51);
  static const Color orangePeel = Color(0xFFFF9E1B);
  static const Color lightRed = Color(0xFFFF9190);
  static const Color silver = Color(0xFFA6A6A6);
  static const Color paleDogwood = Color(0xFFF8DBCF);
  static const Color seaShell = Color(0xFFF5ECE7);
  static const Color mauve = Color(0xFFDAB0F8);
  static const Color powderBlue = Color(0xFFA9B6FC);
  static const Color darkOrange = Color(0xFFF99233);
  static const Color nyanza = Color(0xFFC3EDD8);
  static const Color mindaro = Color(0xFFC9E265);

  static const Color primary = raspberry;
  static const Color secondary = black;
  static const Color tertiary = silver;
  static const Color bottomBar = paleDogwood;
  static const Color background = seaShell;
  static const Color foodCategory = mauve;
  static const Color shopsCategory = powderBlue;
  static const Color transportCategory = darkOrange;
  static const Color healthCategory = lightRed;
  static const Color atmCategory = nyanza;
  static const Color entertainmentCategory = mindaro;
}

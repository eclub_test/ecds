import 'package:ecds/ecds.dart';
import 'package:flutter/material.dart';

part 'types/primary.dart';

class BtnSize {
  const BtnSize({
    required this.paddingH,
    required this.paddingV,
    required this.eClubText,
  });

  final Widget Function({
    Color? color,
    FontWeight? fontWeight,
    Key? key,
    required String label,
    TextAlign? textAlign,
  }) eClubText;
  final double paddingH;
  final double paddingV;
}

class EClubBtnSize {
  EClubBtnSize._();

  static const BtnSize xs = BtnSize(
    paddingH: 10,
    paddingV: 6,
    eClubText: EClubText.small,
  );

  static const BtnSize sm = BtnSize(
    paddingH: 18,
    paddingV: 8,
    eClubText: EClubText.small,
  );
  static const BtnSize sl = BtnSize(
    paddingH: 19,
    paddingV: 12,
    eClubText: EClubText.small,
  );
  static const BtnSize md = BtnSize(
    paddingH: 20,
    paddingV: 15,
    eClubText: EClubText.labelText,
  );
  static const BtnSize mdLittlePadding =
      BtnSize(paddingH: 13, paddingV: 7, eClubText: EClubText.labelText);

  static const BtnSize mdLittlePaddingBold =
      BtnSize(paddingH: 10, paddingV: 5, eClubText: EClubText.labelText);

  static const BtnSize lg = BtnSize(
    paddingH: 24,
    paddingV: 14,
    eClubText: EClubText.body,
  );
  static const BtnSize xl = BtnSize(
    paddingH: 28,
    paddingV: 20,
    eClubText: EClubText.h6,
  );

  static const BtnSize long = BtnSize(
    paddingH: 35,
    paddingV: 5,
    eClubText: EClubText.labelText,
  );
}

class EClubBtnInterface {
  EClubBtnInterface({
    required this.buttonColor,
    required this.labelColor,
    this.assetPackage,
    this.borderColor = Colors.white,
    this.btnBorderRadius = 8.0,
    this.btnSize = EClubBtnSize.md,
    this.hasBorder = false,
    this.labelFontWeight,
    this.showShadow = false,
  });

  Color buttonColor;
  Color labelColor;
  final BtnSize btnSize;
  final Color borderColor;
  final FontWeight? labelFontWeight;
  final String? assetPackage;
  final bool hasBorder;
  final bool showShadow;
  final double btnBorderRadius;
}

class _BtnGeneric extends StatelessWidget {
  const _BtnGeneric({
    super.key,
    required this.btnSize,
    required this.btnType,
    required this.label,
    required this.onTap,
    this.borderRadius,
    this.grayLetters = false,
    this.transparentColorDisabledButton = false,
    this.whiteLetters = false,
  });

  final BorderRadiusGeometry? borderRadius;
  final BtnSize btnSize;
  final GestureTapCallback? onTap;
  final String label;
  final EClubBtnInterface btnType;
  final bool transparentColorDisabledButton;
  final bool grayLetters;
  final bool whiteLetters;

  @override
  Widget build(BuildContext context) {
    return ButtonTheme(
      child: ElevatedButton(
        style: ButtonStyle(
          backgroundColor: MaterialStateProperty.resolveWith<Color?>(
            (Set<MaterialState> states) {
              if (states.contains(MaterialState.disabled)) {
                return transparentColorDisabledButton
                    ? Colors.transparent
                    : whiteLetters
                        ? Ecds.colors.tertiaryColor
                        : btnType.buttonColor == Colors.transparent
                            ? Ecds.colors.tertiaryColor.withOpacity(0.2)
                            : Ecds.colors.tertiaryColor;
              }
              return btnType.buttonColor;
            },
          ),
          shape: MaterialStateProperty.all<RoundedRectangleBorder>(
            RoundedRectangleBorder(
              borderRadius: borderRadius ??
                  BorderRadius.circular(btnType.btnBorderRadius),
              side: btnType.hasBorder
                  ? BorderSide(
                      color: btnType.borderColor,
                    )
                  : BorderSide.none,
            ),
          ),
          shadowColor: MaterialStateProperty.resolveWith<Color?>(
            (Set<MaterialState> states) {
              if (states.contains(MaterialState.disabled)) {
                return btnType.buttonColor == Colors.transparent
                    ? Colors.transparent
                    : Ecds.colors.tertiaryColor.withOpacity(0.6);
              }
              return btnType.buttonColor.withOpacity(0.6);
            },
          ),
          elevation: MaterialStateProperty.all(btnType.showShadow ? 8 : 0),
          padding: MaterialStateProperty.all(
            EdgeInsets.symmetric(
              vertical: btnSize.paddingV,
              horizontal: btnSize.paddingH,
            ),
          ),
          foregroundColor: MaterialStateProperty.all(
            btnType.buttonColor.withOpacity(0.6),
          ),
        ),
        onPressed: onTap,
        child: _text(),
      ),
    );
  }

  Widget _text() {
    return btnSize.eClubText(
      textAlign: TextAlign.center,
      label: label,
      color: onTap == null
          ? whiteLetters
              ? Ecds.colors.tertiaryColor.withOpacity(70)
              : grayLetters
                  ? Ecds.colors.tertiaryColor
                  : Ecds.colors.tertiaryColor.withOpacity(70)
          : btnType.labelColor,
      fontWeight: btnType.labelFontWeight,
    );
  }
}

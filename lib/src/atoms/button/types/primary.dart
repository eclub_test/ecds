part of '../btn.dart';

class _EClubBtnPrimary extends EClubBtnInterface {
  _EClubBtnPrimary({
    required super.labelColor,
    required super.labelFontWeight,
    required super.showShadow,
  }) : super(
          buttonColor: Ecds.colors.primaryColor,
        );
}

class EClubBtnPrimary extends StatelessWidget {
  const EClubBtnPrimary({
    super.key,
    required this.label,
    required this.onTap,
    this.btnSize = EClubBtnSize.md,
    this.labelColor,
    this.labelFontWeight,
    this.showShadow = false,
    this.borderRadius,
  });

  final String label;
  final VoidCallback? onTap;
  final BtnSize btnSize;
  final Color? labelColor;
  final FontWeight? labelFontWeight;
  final bool showShadow;
  final BorderRadiusGeometry? borderRadius;

  @override
  Widget build(BuildContext context) {
    return _BtnGeneric(
      label: label,
      onTap: onTap,
      btnSize: btnSize,
      borderRadius: borderRadius,
      btnType: _EClubBtnPrimary(
        labelColor: labelColor ?? Ecds.colors.white,
        labelFontWeight: labelFontWeight ?? FontWeight.w600,
        showShadow: showShadow,
      ),
    );
  }
}

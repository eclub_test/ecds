import 'package:ecds/ecds.dart';

class Ecds {
  const Ecds._();

  static EClubColorsInterface colors = const EClubColorsInterface();
  static EClubTypographyInterface typography = const EClubTypographyInterface();

  static void init({
    required EClubColorsInterface ecColors,
    required EClubTypographyInterface ecTypography,
  }) {
    colors = ecColors;
    typography = ecTypography;
  }
}
